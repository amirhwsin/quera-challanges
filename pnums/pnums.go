package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	reader := bufio.NewReader(os.Stdin)
	a, err := reader.ReadString('\n')
	if err != nil {
		panic("input error")
	}
	for i := 0; i < len(a)-1; i++ {
		f := string(a[i])
		dofor, err := strconv.ParseInt(string(a[i]), 0, 30)
		if err != nil {
			fmt.Println("Error")
			break
		}
		var m strings.Builder
		for j := 0; j < int(dofor); j++ {
			m.WriteString(string(a[i]))
		}
		s := fmt.Sprintf("%s: %s", f, m.String())
		fmt.Println(s)
	}
}
