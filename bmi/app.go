package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	reader := bufio.NewReader(os.Stdin)
	a, err := reader.ReadString('\n')
	if err != nil {
		panic("input error")
	}
	a = strings.Replace(a, "\n", "", -1)
	i, err := strconv.Atoi(a)
	if err != nil {
		// handle error
		panic(err)
	}

	b, err := reader.ReadString('\n')
	if err != nil {
		panic("input error")
	}
	b = strings.Replace(b, "\n", "", -1)
	i2, err := strconv.ParseFloat(b, 32)
	if err != nil {
		panic(err)
	}
	f := i2 * i2

	bmi := float64(i) / f
	if bmi < 18.5 {
		fmt.Println(bmi)
		fmt.Println("Underweight")
	} else if bmi < 25 {
		fmt.Println(bmi)
		fmt.Println("Normal")
	} else if bmi < 30 {
		fmt.Println(bmi)
		fmt.Println("Overweight")
	} else if 30 < bmi {
		fmt.Println(bmi)
		fmt.Println("Obese")
	}
}
