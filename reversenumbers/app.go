package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

var w []int

func main() {
	reader := bufio.NewReader(os.Stdin)

	for {
		a, err := reader.ReadString('\n')
		if err != nil {
			panic("input error")
		}
		a = strings.Replace(a, "\n", "", -1)
		i, err := strconv.Atoi(a)
		if err != nil {
			// handle error
			panic(err)
		}
		if i == 0 {
			_print(w)
			break
		}
		w = append(w, i)
	}
}

func _print(a []int) {
	for i := len(a) - 1; i >= 0; i-- {
		fmt.Println(a[i])
	}
}
